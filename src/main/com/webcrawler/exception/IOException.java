package com.webcrawler.exception;

public class IOException extends Exception {
	private static final long serialVersionUID = -2232006660537372021L;
	
	public IOException(String message) {
		super(message);
	}

	public IOException(String message, Throwable cause) {
		super(message, cause);
	}
}
