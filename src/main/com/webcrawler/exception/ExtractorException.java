package com.webcrawler.exception;

public class ExtractorException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3655275128396995784L;

	public ExtractorException(String message) {
		super(message);
	}

	public ExtractorException(String message, Throwable cause) {
		super(message, cause);
	}
}
