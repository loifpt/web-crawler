package com.webcrawler.exception;

public class SearchException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3655275348396995784L;

	public SearchException(String message) {
		super(message);
	}

	public SearchException(String message, Throwable cause) {
		super(message, cause);
	}
}
