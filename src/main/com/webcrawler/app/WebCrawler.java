package com.webcrawler.app;

import java.util.ArrayList;
import java.util.List;

import com.webcrawler.entity.HtmlElementGroup;
import com.webcrawler.entity.JSLibraryGroup;
import com.webcrawler.entity.Webpage;
import com.webcrawler.exception.IOException;
import com.webcrawler.exception.SearchException;
import com.webcrawler.helper.GoogleSearchHelper;
import com.webcrawler.helper.IOHelper;
import com.webcrawler.helper.SearchHelper;
import com.webcrawler.helper.StandardIOHelper;
import com.webcrawler.worker.WorkerPool;

public class WebCrawler {

	public static void main(String[] args) throws IOException, SearchException {
		IOHelper ioHelper = new StandardIOHelper();
		String query = ioHelper.getQuery("Please input your query = ");
		
		SearchHelper googleSearchHelper = new GoogleSearchHelper();
		ArrayList<Webpage> webpages = (ArrayList<Webpage>) googleSearchHelper.getWebpageResult(query);
		
		WorkerPool pool = new WorkerPool(webpages);
		pool.execute();
		List<HtmlElementGroup> results = WorkerPool.getResults();
		
		ioHelper.printResult(results);
	}

}
