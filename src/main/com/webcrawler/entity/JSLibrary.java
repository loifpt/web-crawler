package com.webcrawler.entity;

public class JSLibrary extends HtmlElement {
	private String version;
	private long size;
	private String url;

	public JSLibrary() {
		super();
		// TODO Auto-generated constructor stub
	}

	public JSLibrary(String name, String version, String url, long hashValue, long size) {
		super(name, hashValue);
		this.version = version;
		this.size = size;
		this.url = url;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
