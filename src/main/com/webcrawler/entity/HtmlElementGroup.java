package com.webcrawler.entity;

import java.util.List;

public interface HtmlElementGroup {

	<T> void add(T item);

	<T> boolean isInGroup(T item);

	<T> List<T> getItems();

}