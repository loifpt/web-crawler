package com.webcrawler.entity;

/*
 * HtmlElement could be js library, css, class...
 */
public abstract class HtmlElement {
	private String name;
	private long hashValue;

	public HtmlElement() {
		super();
		// TODO Auto-generated constructor stub
	}

	public HtmlElement(String name, long hashValue) {
		super();
		this.name = name;
		this.hashValue = hashValue;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getHashValue() {
		return hashValue;
	}

	public void setHashValue(long hashValue) {
		this.hashValue = hashValue;
	}

}
