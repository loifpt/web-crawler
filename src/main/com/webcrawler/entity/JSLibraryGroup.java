package com.webcrawler.entity;

import java.util.ArrayList;
import java.util.List;

public class JSLibraryGroup implements HtmlElementGroup {
	private List<JSLibrary> items;

	public JSLibraryGroup() {
		super();
		this.items = new ArrayList<>();
	}

	public JSLibraryGroup(JSLibrary item) {
		this.items = new ArrayList<>();
		this.items.add(item);
	}

	/* (non-Javadoc)
	 * @see com.webcrawler.entity.HtmlElementGroup#add(com.webcrawler.entity.JSLibrary)
	 */
	@Override
	public <T> void add(T item) {
		this.items.add((JSLibrary) item);
	}

	/* (non-Javadoc)
	 * @see com.webcrawler.entity.HtmlElementGroup#isInGroup(com.webcrawler.entity.JSLibrary)
	 */
	@Override
	public <T> boolean isInGroup(T item) {
		if (this.items.size() == 0)
			return false;
		return (isEqual((JSLibrary) item, this.items.get(0)));
	}

	/* (non-Javadoc)
	 * @see com.webcrawler.entity.HtmlElementGroup#getItems()
	 */
	@Override
	public List<JSLibrary> getItems() {
		return items;
	}


	/**
	 * Compare hash value & size of js library content to detect if 2 libraries
	 * are equal
	 * 
	 * @param a
	 * @param b
	 * @return boolean
	 */
	private boolean isEqual(JSLibrary a, JSLibrary b) {
		return (a.getSize() == b.getSize() && a.getHashValue() == b.getHashValue());
	}
}
