package com.webcrawler.worker;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.webcrawler.entity.HtmlElementGroup;
import com.webcrawler.entity.JSLibrary;
import com.webcrawler.entity.JSLibraryGroup;
import com.webcrawler.entity.Webpage;

public class WorkerPool {

	private static final int MAX_THREADS = 10;

	List<Webpage> webpages;

	static List<HtmlElementGroup> results;

	public WorkerPool(List<Webpage> webpages) {
		super();
		this.webpages = webpages;
		WorkerPool.results = new ArrayList<>();
	}

	public static synchronized void mergeResults(List<JSLibrary> jsLibs) {

		for (JSLibrary jsLib : jsLibs) {

			boolean isInGroup = false;
			for (HtmlElementGroup jsLibGroup : results) {
				if (jsLibGroup.isInGroup(jsLib)) {
					jsLibGroup.add(jsLib);
					isInGroup = true;
					break;
				}
			}

			if (isInGroup == false) {
				JSLibraryGroup jsLibGroup = new JSLibraryGroup(jsLib);
				results.add(jsLibGroup);
			}
		}
	}

	private void sort(List<HtmlElementGroup> results) {
		for (int i = 0; i < results.size() - 1; i++) {
			for (int j = i + 1; j < results.size(); j++) {
				if (results.get(i).getItems().size() < results.get(j).getItems().size()) {
					HtmlElementGroup temp = results.get(i);
					results.set(i, results.get(j));
					results.set(j, temp);
				}
			}
		}
	}

	public static List<HtmlElementGroup> getResults() {
		return results;
	}

	public void execute() {
		ExecutorService executor = Executors.newFixedThreadPool(MAX_THREADS);

		for (Webpage webpage : webpages) {
			Runnable worker = new JSLibraryWorker(webpage);
			executor.execute(worker);
		}
		executor.shutdown();

		// wait for all workers are completed
		while (!executor.isTerminated()) {
			
		}

		sort(WorkerPool.results);
	}

}