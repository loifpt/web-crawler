package com.webcrawler.worker;

import java.util.List;

import org.apache.log4j.Logger;

import com.webcrawler.entity.JSLibrary;
import com.webcrawler.entity.Webpage;
import com.webcrawler.exception.ExtractorException;
import com.webcrawler.helper.Extractor;
import com.webcrawler.helper.JSLibraryExtractor;

public class JSLibraryWorker implements Runnable {

	final static Logger logger = Logger.getLogger(JSLibraryWorker.class);
	private Webpage webpage;

	public JSLibraryWorker(Webpage webpage) {
		super();
		this.webpage = webpage;
	}

	public Webpage getWebpage() {
		return webpage;
	}

	public void setWebpage(Webpage webpage) {
		this.webpage = webpage;
	}

	@Override
	public void run() {
		logger.info("[Worker-" + Thread.currentThread().getName() + "]: START webpage=" + this.getWebpage().getUrl());

		Extractor extractor = new JSLibraryExtractor();
		try {
			List<JSLibrary> jsLibs = extractor.getHtmlElementFromWebpage(this.getWebpage());
			WorkerPool.mergeResults(jsLibs);

		} catch (ExtractorException e) {
			logger.error("Error in worker: " + Thread.currentThread().getName(), e);
		}

		logger.info("[Worker-" + Thread.currentThread().getName() + "]: FINISH webpage=" + this.getWebpage().getUrl());
	}

}
