package com.webcrawler.helper;

import java.util.List;

import com.webcrawler.entity.HtmlElementGroup;
import com.webcrawler.exception.IOException;

public interface IOHelper {

	public String getQuery(String message) throws IOException;

	void printResult(List<HtmlElementGroup> results);

}