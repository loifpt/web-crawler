package com.webcrawler.helper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.webcrawler.entity.JSLibrary;
import com.webcrawler.entity.Webpage;
import com.webcrawler.exception.ExtractorException;

public class JSLibraryExtractor implements Extractor {
	private static final String JS_LIBRARY_SELECT = "script[type='text/javascript'][src*='/']";
	private static final String USER_AGENT = "Mozilla/5.0";

	public JSLibraryExtractor() {
		super();
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.webcrawler.helper.Extractor#getJSLibraryFromWebpage(com.webcrawler.
	 * entity .Webpage)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<JSLibrary> getHtmlElementFromWebpage(Webpage webpage) throws ExtractorException {

		List<JSLibrary> jsLibs = new ArrayList<>();

		try {
			Document doc = Jsoup.connect(webpage.getUrl()).userAgent(USER_AGENT).header("Content-Type", "text/html")
					.get();

			Elements results = doc.select(JS_LIBRARY_SELECT);

			for (Element result : results) {
				String libUrl = normalizeElementUrl(webpage.getUrl(), result.attr("src"));

				jsLibs.add(getHtmlElementFromUrl(libUrl));
			}
		} catch (IOException ex) {
			throw new ExtractorException("Error in getting js library results from webpage: " + webpage.getUrl(), ex);
		}
		return jsLibs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.webcrawler.helper.Extractor#getJSLibraryFromUrl(java.lang.String)
	 */
	@Override
	public JSLibrary getHtmlElementFromUrl(String libUrl) throws ExtractorException {

		String libName = extractLibName(libUrl);
		String libVersion = extractLibVersion(libUrl);

		try {
			Document doc = Jsoup.connect(libUrl).userAgent(USER_AGENT).ignoreContentType(true).get();
			long hashValue = StringHelper.hash(doc.text());
			long size = doc.text().length();

			return new JSLibrary(libName, libVersion, libUrl, hashValue, size);
		} catch (IOException ex) {
			throw new ExtractorException("Error in getting js library from url: " + libUrl, ex);
		}
	}

	private String extractLibName(String libUrl) {
		// Pattern pattern = Pattern.compile(".*\\/(.*)[\\?]");
		// Matcher matcher = pattern.matcher(libUrl);
		String libName = "";
		if (libUrl.lastIndexOf("?") != -1) {
			libName = libUrl.substring(libUrl.lastIndexOf("/") + 1, libUrl.lastIndexOf("?"));
		} else {
			libName = libUrl.substring(libUrl.lastIndexOf("/") + 1);
		}

		return libName;
	}

	private String extractLibVersion(String libUrl) {
		String libVersion = "";
		if (libUrl.lastIndexOf("ver=") != -1) {
			libVersion = libUrl.substring(libUrl.lastIndexOf("ver=") + 4);
		}

		return libVersion;
	}

	private String normalizeElementUrl(String hostUrl, String elementUrl) {
		if (elementUrl == null || elementUrl.isEmpty())
			return "";

		if (elementUrl.startsWith("http")) {
			//keep value
		} else if (elementUrl.startsWith("//")) {
			if (hostUrl.startsWith("https:")) {
				elementUrl = "https:" + elementUrl;
			} else {
				elementUrl = "http:" + elementUrl;
			}
		} else if (elementUrl.startsWith("/")) {
			elementUrl = hostUrl + elementUrl;
		} else {
			if (hostUrl.startsWith("https:")) {
				elementUrl = "https://" + elementUrl;
			} else {
				elementUrl = "http://" + elementUrl;
			}
		}

		return elementUrl;
	}

}
