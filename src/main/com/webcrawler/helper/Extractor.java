package com.webcrawler.helper;

import java.util.List;

import com.webcrawler.entity.HtmlElement;
import com.webcrawler.entity.Webpage;
import com.webcrawler.exception.ExtractorException;

public interface Extractor {

	public <T> List<T> getHtmlElementFromWebpage(Webpage webpage) throws ExtractorException;

	public HtmlElement getHtmlElementFromUrl(String url) throws ExtractorException;

}