package com.webcrawler.helper;

import java.util.List;

import org.jsoup.nodes.Document;

import com.webcrawler.entity.Webpage;
import com.webcrawler.exception.SearchException;

public interface SearchHelper {

	List<Webpage> getWebpageResult(String query) throws SearchException;

	List<Webpage> getWebpageResult(Document document);

}