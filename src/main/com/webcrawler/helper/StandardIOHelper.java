package com.webcrawler.helper;

import java.util.List;
import java.util.Scanner;

import com.webcrawler.entity.HtmlElementGroup;
import com.webcrawler.entity.JSLibrary;
import com.webcrawler.exception.IOException;

public class StandardIOHelper implements IOHelper {
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.webcrawler.util.IOUtil#getQuery(java.lang.String)
	 */
	@Override
	public String getQuery(String message) throws IOException {
		Scanner sc = new Scanner(System.in);
		String query = "";
		try {
			System.out.print(message);
			query = sc.next();
		} catch (Exception ex) {
			throw new IOException("Error in getting query string from Standard IO", ex);
		} finally {
			sc.close();
		}
		return query;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.webcrawler.util.IOUtil#printResult(java.util.List)
	 */

	@Override
	public void printResult(List<HtmlElementGroup> results) {
		System.out.println("---------[ TOP 5 MOST USED JAVASCRIPT LIBRARIES ]--------------------");
		System.out.println("** All javascript libraries in the same group are duplicated"); 
		for (int i=0; (i < results.size() && i < 5); i++) {
			System.out.println("Group " + (i + 1) + " (total: " + results.get(i).getItems().size() + ")");
			for (Object item : results.get(i).getItems()) {
				JSLibrary jsLib = (JSLibrary) item;
				System.out.println("(" + jsLib.getName() + ", " + jsLib.getVersion() + ") " + jsLib.getUrl());
			}
			System.out.println();
		}

	}
}
