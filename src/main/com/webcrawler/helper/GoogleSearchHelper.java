package com.webcrawler.helper;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.webcrawler.entity.Webpage;
import com.webcrawler.exception.SearchException;

public class GoogleSearchHelper implements SearchHelper {

	private static final String GOOGLE_SEARCH_URL = "https://www.google.com/search";
	private static final String GOOGLE_LINK_SELECT = "h3.r > a";
	private static final String USER_AGENT = "Mozilla/5.0";
	private static final String QUERY_CHARSET = "iso-8859-1";

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.webcrawler.helper.SearchHelper#getWebpageResult(java.lang.String)
	 */
	@Override
	public List<Webpage> getWebpageResult(Document document) {
		List<Webpage> webpages = new ArrayList<>();
		Elements results = document.select(GOOGLE_LINK_SELECT);

		for (Element result : results) {
			String pageUrl = extractUrl(result.attr("href"));
			if (!pageUrl.isEmpty()) {
				String pageTitle = result.text();
				Webpage webpage = new Webpage(pageTitle, pageUrl);
				webpages.add(webpage);
			}
		}

		return webpages;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.webcrawler.helper.SearchHelper#getWebpageResult(java.lang.String)
	 */
	@Override
	public List<Webpage> getWebpageResult(String query) throws SearchException {
		String address = GOOGLE_SEARCH_URL + "?q=";
		try {
			Document doc = Jsoup.connect(address + URLEncoder.encode(query, QUERY_CHARSET)).userAgent(USER_AGENT).get();
			return getWebpageResult(doc);
		} catch (IOException ex) {
			throw new SearchException("Error in getting google webpage results", ex);
		}
	}
	
	private String extractUrl(String rawText) {	
		rawText = java.net.URLDecoder.decode(rawText);
		if (rawText != null && !rawText.isEmpty()) {
			rawText = rawText.substring(7, rawText.indexOf("&"));
		}
		if (rawText.startsWith("http")) {
			return rawText;
		} else {
			return "";
		}
	}

}