package com.webcrawler.helper;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.webcrawler.entity.JSLibrary;
import com.webcrawler.exception.ExtractorException;

public class JSLibraryExtractorTest {

	JSLibraryExtractor extractor;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		extractor = new JSLibraryExtractor();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetHtmlElementFromWebpage() {
	}

	@Test
	public void testGetHtmlElementFromUrl() throws ExtractorException, IOException {
		String testUrl = "https://zgab33vy595fw5zq-zippykid.netdna-ssl.com/wp-includes/js/wp-embed.min.js?ver=4.9.6";
		Document doc = Jsoup.connect(testUrl).userAgent("Mozilla/5.0").ignoreContentType(true).get();
		long hashValue = StringHelper.hash(doc.text());
		long size = doc.text().length();

		JSLibrary expected = new JSLibrary("wp-embed.min.js", "4.9.6", testUrl, hashValue, size);

		JSLibrary actual = extractor.getHtmlElementFromUrl(testUrl);

				
		assertEquals(expected.getName(), actual.getName());
		assertEquals(expected.getVersion(), actual.getVersion());
		assertEquals(expected.getUrl(), actual.getUrl());
		assertEquals(expected.getHashValue(), actual.getHashValue());
		assertEquals(expected.getSize(), actual.getSize());

	}
	

}
