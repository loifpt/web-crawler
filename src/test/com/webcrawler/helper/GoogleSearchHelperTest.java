package com.webcrawler.helper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.webcrawler.entity.Webpage;
import com.webcrawler.exception.SearchException;

public class GoogleSearchHelperTest {

	SearchHelper searchHelper;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		searchHelper = new GoogleSearchHelper(); 
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetWebpageResultDocument() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetWebpageResultString() throws SearchException {
		List<Webpage> webpages = searchHelper.getWebpageResult("business process management");
		for (Webpage webpage : webpages) {
			System.out.println(webpage.getUrl());
		}
	}
	
	@Test
	public void testExtractUrl() throws SearchException {
		String expectedValue = "/url?q=https://en.wikipedia.org/wiki/Business_process_management&sa=U&ved=0ahUKEwjW4-zo0-7bAhXBTLwKHZtDB88QFggSMAA&usg=AOvVaw1l3oxxiCyMSoNbd2TeRj4B";
		//String actualvalue = searchHelper.e /url?q=https://en.wikipedia.org/wiki/Business_process_management&sa=U&ved=0ahUKEwjW4-zo0-7bAhXBTLwKHZtDB88QFggSMAA&usg=AOvVaw1l3oxxiCyMSoNbd2TeRj4B
		//assertEquals(expected, actual);
	}

}
