package com.webcrawler.helper;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.webcrawler.exception.IOException;

public class StandardIOHelperTest {

	StandardIOHelper ioHelper;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		ioHelper = new StandardIOHelper();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetQuery() throws IOException {
		String expected = "test";

		InputStream fakeIn = new ByteArrayInputStream(expected.getBytes());
		System.setIn(fakeIn);

		String actual = ioHelper.getQuery("Test getting query string :");
		
		assertEquals(expected, actual);	
		
	}

	@Test
	public void testPrintResult() {
		
	}

}
